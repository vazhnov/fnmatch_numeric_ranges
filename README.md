# fnmatch_numeric_ranges.py

Simple python library/script.

You can use it like shell script or like a library.

Based on (and require) [fnmatch](https://docs.python.org/3/library/fnmatch.html).

## Shell script

### Syntax

```shell
fnmatch_numeric_ranges.py pattern name1 [name2] [name3] …
```

### Usage example

```shell
ls | xargs fnmatch_numeric_ranges.py "IMG_*_[185000:185959].jpg"
```

## Library

### Usage example

```python
#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""Example for library fnmatch_numeric_ranges"""

from __future__ import print_function
import fnmatch_numeric_ranges

MYLIST = [
    'IMG_20160826_185735.jpg',
    'IMG_20160826_185741.jpg',
    'IMG_20160826_185801.jpg',
    'IMG_20160826_190603.jpg'
    ]

for i in MYLIST:
    print(fnmatch_numeric_ranges.fnmatch_ranges(i, "IMG_*_[185730:185750].jpg"))
```

## Known bugs

* Only one numeric range in pattern

## See also:

* Ansible's [expand\_hostname\_range](https://github.com/ansible/ansible/blob/devel/lib/ansible/plugins/inventory/__init__.py#L59)

## Copyright

[GNU Lesser General Public License](https://www.gnu.org/copyleft/lesser.html) v3 or newer.
