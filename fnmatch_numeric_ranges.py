#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""Like fnmatch, but with numeric ranges in square brackets.

You can use it like shell script.

Syntax:
fnmatch_numeric_ranges.py pattern name1 [name2] [name3] …

Usage example:
$ls | xargs fnmatch_numeric_ranges.py "IMG_*_[185000:185959].jpg"

See more information in https://docs.python.org/3/library/fnmatch.html
"""

# pylint: disable=line-too-long,missing-docstring

from __future__ import print_function
import fnmatch
import re
import sys


def fnmatch_ranges(name, pattern):
    """Match string to pattern.

    With this library you can use all standard "fnmatch" patterns for matching:
    *      — matches everything
    ?      — matches any single character
    [seq]  — matches any character in seq
    [!seq] — matches any character not in seq

    and numeric ranges in square brackets:
    [num1:num2] — matches any number between num1 and num2, include both of them.
    """
    # groups in match1:            1   [  2   :  3   ] 4
    re_num_range_sq_brackets = r'^(.*)\[(\d*)\:(\d*)\](.*)$'
    match1 = re.search(re_num_range_sq_brackets, pattern)
    if match1:
        # We have numeric ranges in square brackets

        # Going to recurse
        #new_pattern = match1.group(1) + r'(\d+)' + match1.group(4)
        #match2 = re.search(re_num_range_sq_brackets, match1)
        #if match2:
        #    fnmatch_ranges(name, new_pattern)

        # This part is for working like fnmatch (use *, ?, …), but use range of numbers
        # Removing '\Z(?ms)' from the end of first regex, because this is not the end here
        translated_regex1 = fnmatch.translate(match1.group(1))[:-7]
        translated_regex2 = fnmatch.translate(match1.group(4))
        translated_regex_full = translated_regex1 + r'(\d+)' + translated_regex2
        match3 = re.search(translated_regex_full, name)
        if match3:
            if int(match3.group(1)) >= int(match1.group(2)) and int(match3.group(1)) <= int(match1.group(3)):
                return name
    else:
        # We doesn't have numeric ranges in square brackets
        if fnmatch.fnmatch(name, pattern):
            return name


def filter_ranges(strings, pattern):
    """Return list of strings that match pattern."""
    result = []
    for string in strings:
        if fnmatch_ranges(string, pattern):
            result.append(string)
    return result


def main():
    pattern = sys.argv[1]
    for arg_num in range(2, len(sys.argv)):
        name = fnmatch_ranges(sys.argv[arg_num], pattern)
        if name:
            print(name)


if __name__ == "__main__":
    main()
